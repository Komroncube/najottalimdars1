import os
os.system("cls")

def binaryexp(x, n):
    result = 1
    while n > 0:
        if n % 2 == 1:
            result = (result * x) % 1000000007
        x = (x * x) % 1000000007
        n //= 2
    return result

n, k = map(int, input().split())
if n == 0:
    print(1)
else:
    result = binaryexp(k + 1, n)
    print(result)